import "./Featured.css";
import Featured_1 from "../../assets/image/Featured1.jpg";
import Featured_2 from "../../assets/image/Featured.jpg";
import Featured_3 from "../../assets/image/Featured3.jpg";
import Featured_4 from "../../assets/image/Featured4.jpg";
import useFetch from "../../hooks/useFetch";

const Featured = () => {
  const { data, loading, error } = useFetch(
    "/houses/countByCity?cities=HCM,Da Nang,Ha Noi,Can Tho"
  );

  return (
    <div className="featured">
      {loading ? (
        "Loading please wait"
      ) : (
        <>
          <div className="featuredItem">
            <img src={Featured_1} alt="" className="featuredImage" />
            <div className="featuredTitle">
              <h1>HCM</h1>
              <h2>{data[0]} Properties</h2>
            </div>
          </div>
          <div className="featuredItem">
            <img src={Featured_2} alt="" className="featuredImage" />
            <div className="featuredTitle">
              <h1>Da Nang</h1>
              <h2>{data[1]} Properties</h2>
            </div>
          </div>
          <div className="featuredItem">
            <img src={Featured_3} alt="" className="featuredImage" />
            <div className="featuredTitle">
              <h1>Ha Noi</h1>
              <h2>{data[2]} Properties</h2>
            </div>
          </div>
          <div className="featuredItem">
            <img src={Featured_4} alt="" className="featuredImage" />
            <div className="featuredTitle">
              <h1>Can Tho</h1>
              <h2>{data[3]} Properties</h2>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Featured;
