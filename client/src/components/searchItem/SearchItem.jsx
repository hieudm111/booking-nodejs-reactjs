import "./SearchItem.css";
import { Link } from "react-router-dom";

const SearchItem = ({ item }) => {
  return (
    <div className="searchItem">
      <img src={item.photos[0]} alt="" className="siImage" />
      <div className="siDesc">
        <h1 className="siTitle">{item.name}</h1>
        <span className="siDistance">{item.distance}m from center</span>
        <div className="siService">
          <span className="siServiceOp">Free wifi</span>
          <span className="siServiceOp">Free parking</span>
        </div>
        <span className="siSubtitle">
          Studio Apartment with Air conditioning
        </span>
        <span className="siFeaturerd">{item.desc}</span>
        <span className="siCancelOp">Free cancellation</span>
        <span className="siCancelSubtilte">
          You can cancel later, so lock in this great price today!
        </span>
      </div>
      <div className="siDetails">
        {item.rating && (
          <div className="siRating">
            <span>Excellent</span>
            <button>{item.rating}</button>
          </div>
        )}
        <div className="siDetailTexts">
          <span className="siPrice">${item.cheapestPrice}</span>
          <span className="siTaxOp">Includes taxes and fees</span>
          <Link to={`/boarding-house/${item._id}`}>
            <button className="siCheckButton">See availability</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SearchItem;
