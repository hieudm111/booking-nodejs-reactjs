import "./Navbar.css";
import Logo from "../../assets/image/logo.png";
import { Link, useNavigate } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../../context/AuthContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRightFromBracket } from "@fortawesome/free-solid-svg-icons";

const Navbar = () => {
  const { user, dispatch } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleLogout = () => {
    try {
      dispatch({ type: "LOGOUT" });
      localStorage.removeItem("token");
      console.log("Logout Successfully!");
      navigate("/login");
    } catch (error) {
      return error;
    }
  };

  return (
    <div className="navbar">
      <div className="navContainer">
        <Link to={"/"} style={{ color: "inherit", textDecoration: "none" }}>
          <span className="logo">
            <img src={Logo} alt="" />
          </span>
        </Link>
        {user ? (
          <span
            style={{
              color: "white",
              fontSize: "20px",
              border: "2px solid rgba(255, 255, 255, 0.2)",
              padding: "10px 20px",
              display: "flex",
              gap: "10px",
            }}
          >
            {user.username}
            <button
              onClick={handleLogout}
              style={{
                border: "none",
                cursor: "pointer",
                background: "none",
                color: "white",
                fontSize: "18px",
                padding: "0",
              }}
            >
              <FontAwesomeIcon
                icon={faRightFromBracket}
                className="headerlogout_icon"
              />
            </button>
          </span>
        ) : (
          <div className="navItems">
            <Link
              to={"/register"}
              style={{ color: "inherit", textDecoration: "none" }}
            >
              <button className="navButton">Register</button>
            </Link>
            <Link
              to={"/login"}
              style={{ color: "inherit", textDecoration: "none" }}
            >
              <button className="navButton">Login</button>
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};

export default Navbar;
