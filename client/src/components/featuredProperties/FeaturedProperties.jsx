import "./FeaturedProperties.css";
import useFetch from "../../hooks/useFetch";
import { useState } from "react";

const FeaturedProperties = () => {
  const { data, loading, error } = useFetch("/houses?featured=true");
  const [list, setList] = useState(3);

  return (
    <div className="fp">
      {loading ? (
        "Loading"
      ) : (
        <>
          {data.map((item, index) => {
            if (index > list) {
              return null;
            } else if (item?.featured) {
              return (
                <div className="fpItem" key={item._id}>
                  <img src={item.photos[0]} alt="" className="fpImage" />
                  <span className="fpName">{item.name}</span>
                  <span className="fpCity">{item.city}</span>
                  <span className="fpPrice">
                    Starting from ${item.cheapestPrice}
                  </span>
                  {item.rating && (
                    <div className="fpRating">
                      <button>{item.rating}</button>
                      <span>Excellent</span>
                    </div>
                  )}
                </div>
              );
            } else {
              return null;
            }
          })}
        </>
      )}
    </div>
  );
};

export default FeaturedProperties;
