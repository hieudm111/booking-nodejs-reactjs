import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/home/Home";
import List from "./pages/list/List";
import BoardingHouse from "./pages/house/BoardingHouse";
import axios from "axios";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";

axios.defaults.baseURL = "http://localhost:8800/api";
axios.defaults.withCredentials = true;

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/boarding-houses" element={<List />} />
        <Route path="/boarding-house/:id" element={<BoardingHouse />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
