import "./Register.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { FaUser, FaLock } from "react-icons/fa";
import { MdEmail } from "react-icons/md";
import { IoIosClose } from "react-icons/io";
import axios from "axios";

const Register = () => {
  const [credentials, setCredentials] = useState({
    username: undefined,
    email: undefined,
    password: undefined,
  });

  const [displayError, setDisplayError] = useState(false);
  const [displayErrorBlank, setDisplayErrorBlank] = useState(false);
  const [displayErrorEmail, setDisplayErrorEmail] = useState(false);
  const [displayErrorPass, setDisplayErrorPass] = useState(false);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const navigate = useNavigate();

  const handleChange = (e) => {
    setCredentials((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleClick = async (e) => {
    e.preventDefault();
    const { username, email, password } = credentials;
    const emailRegex = /^[^s@]+@[^s@]+.[^s@]+$/;
    const passwordRegex =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,16}$/;
    if (!username || !email || !password) {
      setDisplayErrorBlank(true);
      return;
    }

    if (!emailRegex.test(email) || !email.endsWith("@gmail.com")) {
      setDisplayErrorEmail(true);
      return;
    }

    if (!passwordRegex.test(password)) {
      setDisplayErrorPass(true);
      return;
    }

    try {
      const res = await axios.post("/auth/register", credentials);
      if (res) {
        setDisplaySuccess(true);
        setTimeout(() => {
          navigate("/login");
        }, 5000);
      } else {
        setDisplayError(true);
      }
    } catch (err) {
      setDisplayError(true);
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleLogin();
    }
  };

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 3000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayErrorBlank) {
      setTimeout(() => {
        setDisplayErrorBlank(false);
      }, 3000);
    }
  }, [displayErrorBlank]);
  useEffect(() => {
    if (displayErrorEmail) {
      setTimeout(() => {
        setDisplayErrorEmail(false);
      }, 3000);
    }
  }, [displayErrorEmail]);
  useEffect(() => {
    if (displayErrorPass) {
      setTimeout(() => {
        setDisplayErrorPass(false);
      }, 3000);
    }
  }, [displayErrorPass]);

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 3000);
    }
  }, [displaySuccess]);

  return (
    <div className="registerForm">
      <div
        style={{
          minWidth: "25%",
          position: "absolute",
          backgroundColor: "red",
          padding: "20px",
          right: "2.5%",
          top: "21%",
          display: displayErrorBlank ? "flex" : "none",
          justifyContent: "space-between",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <div
          style={{
            minHeight: "100%",
            display: "flex",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <span
            style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
          >
            Error:
          </span>
          <span style={{ color: "white" }}>All field are required!</span>
        </div>
        <button
          onClick={() => setDisplayErrorBlank(false)}
          style={{
            cursor: "pointer",
            border: "none",
            padding: "0",
            background: "none",
            fontSize: "30px",
            display: "flex",
            justifyContent: "flex-end",
            color: "white",
          }}
        >
          <IoIosClose />
        </button>
      </div>

      <div
        style={{
          minWidth: "25%",
          position: "absolute",
          backgroundColor: "red",
          padding: "20px",
          right: "2.5%",
          top: "17%",
          display: displayErrorPass ? "flex" : "none",
          justifyContent: "space-between",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <div
          style={{
            minHeight: "100%",
          }}
        >
          <div
            style={{ display: "flex", flexDirection: "column", gap: "15px" }}
          >
            <span
              style={{
                color: "white",
                fontWeight: "bold",
                fontSize: "18px",
                marginBottom: "10px",
              }}
            >
              Invalid Password:
            </span>
            <span style={{ color: "white", fontSize: "17px" }}>
              Password must have at least:{" "}
            </span>
          </div>
          <ul style={{ color: "white" }}>
            <li>One lowercase letter</li>
            <li>One uppercase letter</li>
            <li>One number</li>
            <li>One special letter</li>
            <li>From 8 to 16 characters</li>
          </ul>
        </div>
        <button
          onClick={() => setDisplayErrorPass(false)}
          style={{
            cursor: "pointer",
            border: "none",
            padding: "0",
            background: "none",
            fontSize: "30px",
            display: "flex",
            justifyContent: "flex-end",
            color: "white",
          }}
        >
          <IoIosClose />
        </button>
      </div>

      <div
        style={{
          minWidth: "25%",
          position: "absolute",
          backgroundColor: "red",
          padding: "20px",
          right: "2.5%",
          top: "21%",
          display: displayErrorEmail ? "flex" : "none",
          justifyContent: "space-between",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <div
          style={{
            minHeight: "100%",
            display: "flex",
            alignItems: "flex-start",
            gap: "10px",
          }}
        >
          <div
            style={{ display: "flex", flexDirection: "column", gap: "10px" }}
          >
            <span
              style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
            >
              Invalid Email:
            </span>
            <span style={{ color: "white" }}>
              Wrong format email
              <br />
              Example: example@gmail.com
            </span>
          </div>
        </div>
        <button
          onClick={() => setDisplayErrorEmail(false)}
          style={{
            cursor: "pointer",
            border: "none",
            padding: "0",
            background: "none",
            fontSize: "30px",
            display: "flex",
            justifyContent: "flex-end",
            color: "white",
          }}
        >
          <IoIosClose />
        </button>
      </div>

      <div
        style={{
          minWidth: "25%",
          position: "absolute",
          backgroundColor: "red",
          padding: "20px",
          right: "2.5%",
          top: "21%",
          display: displayError ? "flex" : "none",
          justifyContent: "space-between",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <div
          style={{
            minHeight: "100%",
            display: "flex",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <span
            style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
          >
            Error:
          </span>
          <span style={{ color: "white" }}>{setDisplayError}</span>
        </div>
        <button
          onClick={() => setDisplayError(false)}
          style={{
            cursor: "pointer",
            border: "none",
            padding: "0",
            background: "none",
            fontSize: "30px",
            display: "flex",
            justifyContent: "flex-end",
            color: "white",
          }}
        >
          <IoIosClose />
        </button>
      </div>

      <div
        style={{
          minWidth: "25%",
          position: "absolute",
          backgroundColor: "green",
          padding: "20px",
          right: "2.5%",
          top: "21%",
          display: displaySuccess ? "flex" : "none",
          justifyContent: "space-between",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <div
          style={{
            minHeight: "100%",
            display: "flex",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <span
            style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
          >
            Success:
          </span>
          <span style={{ color: "white" }}>Register Successfully!</span>
        </div>
        <button
          onClick={() => setDisplaySuccess(false)}
          style={{
            cursor: "pointer",
            border: "none",
            padding: "0",
            background: "none",
            fontSize: "30px",
            display: "flex",
            justifyContent: "flex-end",
            color: "white",
          }}
        >
          <IoIosClose />
        </button>
      </div>
      <div className="wrapper">
        <form onKeyDown={handleKeyPress}>
          <h1>Register</h1>
          <div className="input-box">
            <input
              onChange={handleChange}
              className="input-text"
              id="username"
              type="text"
              placeholder="Username"
              required
            />
            <FaUser className="icons" />
          </div>
          <div className="input-box">
            <input
              onChange={handleChange}
              className="input-text"
              id="email"
              type="email"
              placeholder="Email"
              required
            />
            <MdEmail className="icons" />
          </div>
          <div className="input-box">
            <input
              onChange={handleChange}
              className="input-text"
              id="password"
              type="password"
              placeholder="Password"
              required
            />
            <FaLock className="icons" />
          </div>
          <button onClick={handleClick} type="button">
            Register
          </button>
          <div className="login-link">
            <p>
              All ready have an account? <a href="/login">Login here</a>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;
