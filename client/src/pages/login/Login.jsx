import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Login.css";
import { FaUser, FaLock } from "react-icons/fa";
import { IoIosClose } from "react-icons/io";
import { AuthContext } from "../../context/AuthContext";
import axios from "axios";
const Login = () => {
  const [credentials, setCredentials] = useState({
    username: undefined,
    password: undefined,
  });

  const [displayError, setDisplayError] = useState(false);
  const [displaySuccess, setDisplaySuccess] = useState(false);
  const navigate = useNavigate();

  const { loading, error, dispatch } = useContext(AuthContext);

  const handleChange = (e) => {
    setCredentials((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleClick = async (e) => {
    e.preventDefault();
    dispatch({ type: "LOGIN_START" });
    try {
      const res = await axios.post("/auth/login", credentials);
      if (res) {
        dispatch({ type: "LOGIN_SUCCESS", payload: res.data.details });
        setDisplaySuccess(true);
        setTimeout(() => {
          navigate("/");
        }, 5000);
      } else {
        setDisplayError(true);
      }
    } catch (err) {
      dispatch({ type: "LOGIN_FAILURE", payload: err.response.data });
      setDisplayError(true);
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleClick();
    }
  };

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 3000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 3000);
    }
  }, [displaySuccess]);

  return (
    <div className="loginForm">
      <div
        style={{
          minWidth: "25%",
          position: "absolute",
          backgroundColor: "red",
          padding: "20px",
          right: "2.5%",
          top: "21%",
          display: displayError ? "flex" : "none",
          justifyContent: "space-between",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <div
          style={{
            minHeight: "100%",
            display: "flex",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <span
            style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
          >
            Error:
          </span>
          {error && <span style={{ color: "white" }}>{error.message}</span>}
        </div>
        <button
          onClick={() => setDisplayError(false)}
          style={{
            cursor: "pointer",
            border: "none",
            padding: "0",
            background: "none",
            fontSize: "30px",
            display: "flex",
            justifyContent: "flex-end",
            color: "white",
          }}
        >
          <IoIosClose />
        </button>
      </div>

      <div
        style={{
          minWidth: "25%",
          position: "absolute",
          backgroundColor: "green",
          padding: "20px",
          right: "2.5%",
          top: "21%",
          display: displaySuccess ? "flex" : "none",
          justifyContent: "space-between",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <div
          style={{
            minHeight: "100%",
            display: "flex",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <span
            style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
          >
            Success:
          </span>
          <span style={{ color: "white" }}>Login Successfully!</span>
        </div>
        <button
          onClick={() => setDisplaySuccess(false)}
          style={{
            cursor: "pointer",
            border: "none",
            padding: "0",
            background: "none",
            fontSize: "30px",
            display: "flex",
            justifyContent: "flex-end",
            color: "white",
          }}
        >
          <IoIosClose />
        </button>
      </div>
      <div className="wrapper">
        <form onKeyPress={handleKeyPress}>
          <h1>Login</h1>
          <div className="input-box">
            <input
              onChange={handleChange}
              className="input-text"
              id="username"
              type="text"
              placeholder="Username"
              required
            />
            <FaUser className="icons" />
          </div>
          <div className="input-box">
            <input
              onChange={handleChange}
              className="input-text"
              id="password"
              type="password"
              placeholder="Password"
              required
            />
            <FaLock className="icons" />
          </div>
          <div className="remember-forgot">
            <label>
              <input type="checkbox" />
              Remember me
            </label>
            <a href="#">Forgot password?</a>
          </div>
          <button disabled={loading} onClick={handleClick} type="button">
            Login
          </button>
          <div className="register-link">
            <p>
              Don't have an account?<a href="/register">Register</a>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
