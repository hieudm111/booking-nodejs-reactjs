import Room from "../models/Room.js";
import House from "../models/House.js";

export const createRoom = async (req, res, next) => {
  const houseId = req.params.houseid;
  const newRoom = new Room(req.body);

  try {
    const saveRoom = await newRoom.save();
    const check = await House.findByIdAndUpdate(houseId, {
      $push: { rooms: saveRoom._id },
    });
    if (check) {
      res.status(200).json(saveRoom);
      return true;
    } else {
      return false;
    }
  } catch (err) {
    next(err);
  }
};

export const updateRoom = async (req, res, next) => {
  try {
    const updatedRoom = await Room.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedRoom);
  } catch (err) {
    next(err);
  }
};

export const updateRoomAvailability = async (req, res, next) => {
  try {
    const { checkIn, checkOut } = req.body;
    if (checkIn === checkOut) {
      await Room.updateOne(
        { "roomNumbers._id": req.params.id },
        {
          $set: {
            "roomNumbers.$[elem].unavailabelDates": [],
          },
        },
        { arrayFilters: [{ "elem._id": req.params.id }], new: true }
      );
    }
    const update = await Room.updateOne(
      { "roomNumbers._id": req.params.id },
      {
        $push: {
          "roomNumbers.$[elem].unavailabelDates": req.body.dates,
        },
      },
      { arrayFilters: [{ "elem._id": req.params.id }], new: true }
    );
    if (update.nModified > 0) {
      res.status(200).json("Room status has been updated.");
      return true;
    } else {
      res.status(404).json("Room has not found!");
    }
  } catch (err) {
    next(err);
  }
};

export const deleteRoom = async (req, res, next) => {
  const houseId = req.params.houseid;

  try {
    await Room.findByIdAndDelete(req.params.id);
    try {
      await House.findByIdAndUpdate(houseId, {
        $pull: { rooms: req.params.id },
      });
    } catch (err) {
      next(err);
    }
    res.status(200).json("Room has been deleted.");
  } catch (err) {
    next(err);
  }
};
export const getRoom = async (req, res, next) => {
  try {
    const room = await Room.findById(req.params.id);
    res.status(200).json(room);
  } catch (err) {
    next(err);
  }
};
export const getAllRoom = async (req, res, next) => {
  try {
    const rooms = await Room.find();
    res.status(200).json(rooms);
  } catch (err) {
    next(err);
  }
};
