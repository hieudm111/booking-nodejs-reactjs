import House from "../models/House.js";
import Room from "../models/Room.js";
import fs from "fs";

export const createHouse = async (req, res, next) => {
  const newHouse = new House(req.body);
  try {
    const savedHouse = await newHouse.save();
    if (savedHouse) {
      res.status(200).json(savedHouse);
      return true;
    } else {
      return false;
    }
  } catch (err) {
    next(err);
  }
};
export const updateHouse = async (req, res, next) => {
  try {
    const updatedHouse = await House.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedHouse);
  } catch (err) {
    next(err);
  }
};
export const deleteHouse = async (req, res, next) => {
  try {
    await House.findByIdAndDelete(req.params.id);
    res.status(200).json("House has been deleted.");
  } catch (err) {
    next(err);
  }
};
export const getHouse = async (req, res, next) => {
  try {
    const house = await House.findById(req.params.id);
    res.status(200).json(house);
  } catch (err) {
    next(err);
  }
};

export const getAllHouse = async (req, res, next) => {
  const { min, max, ...others } = req.query;
  try {
    const houses = await House.find({
      ...others,
      cheapestPrice: { $gt: min | 1, $lt: max || 999 },
    }).limit(req.query.limit);
    res.status(200).json(houses);
  } catch (err) {
    next(err);
  }
};

export const countByCity = async (req, res, next) => {
  const cities = req.query.cities.split(",");
  try {
    const list = await Promise.all(
      cities.map((city) => {
        return House.countDocuments({ city: city });
      })
    );
    res.status(200).json(list);
  } catch (err) {
    next(err);
  }
};

export const countByType = async (req, res, next) => {
  try {
    const apartmentCount = await House.countDocuments({ type: "Hotel" });
    const houseCount = await House.countDocuments({ type: "BoardingHouse" });
    const resortcount = await House.countDocuments({ type: "Resort" });
    const homestayCount = await House.countDocuments({ type: "HomeStay" });
    const villaCount = await House.countDocuments({ type: "Villa" });
    res.status(200).json([
      { type: "Hotel", count: apartmentCount },
      { type: "BoardingHouse", count: houseCount },
      { type: "Resort", count: resortcount },
      { type: "HomeStay", count: homestayCount },
      { type: "Villa", count: villaCount },
    ]);
  } catch (err) {
    next(err);
  }
};

export const getHouseRooms = async (req, res, next) => {
  try {
    const house = await House.findById(req.params.id);
    const list = await Promise.all(
      house.rooms.map((room) => {
        return Room.findById(room);
      })
    );
    return res.status(200).json(list);
  } catch (err) {
    next(err);
  }
};

export const uploadImage = async (req, res, next) => {
  try {
    const uploadedFiles = [];
    for (let i = 0; i < req.files.length; i++) {
      const { path, originalname } = req.files[i];
      const parts = originalname.split(".");
      const ext = parts[parts.length - 1];
      const newPath = path + "." + ext;

      await fs.renameSync(path, newPath);

      uploadedFiles.push(newPath.replace("uploads", ""));
      res.status(200).json(uploadedFiles);
    }
    if (uploadedFiles.length > 0) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    next(err);
  }
};
