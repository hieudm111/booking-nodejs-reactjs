import express from "express";
import {
  countByCity,
  countByType,
  createHouse,
  deleteHouse,
  getAllHouse,
  getHouse,
  getHouseRooms,
  updateHouse,
  uploadImage,
} from "../controllers/house.js";

import { verifyAdmin } from "../utils/verifyToken.js";
import multer from "multer";
const photosMiddleware = multer({ dest: "uploads" });

const router = express.Router();

//CREATE
router.post("/", verifyAdmin, createHouse);

//UPLOAD-IMAGE
router.post(
  "/upload",
  photosMiddleware.array("photos", 100),
  verifyAdmin,
  uploadImage
);

//UPDATE
router.put("/:id", verifyAdmin, updateHouse);

//DELETE
router.delete("/:id", verifyAdmin, deleteHouse);

//GET
router.get("/find/:id", getHouse);

//GET ALL
router.get("/", getAllHouse);
router.get("/countByCity", countByCity);
router.get("/countByType", countByType);
router.get("/room/:id", getHouseRooms);

export default router;
