export const userInputs = [
  {
    id: "username",
    label: "Username",
    type: "text",
    placeholder: "user_example",
  },
  {
    id: "email",
    label: "Email",
    type: "email",
    placeholder: "example@gmail.com",
  },
  {
    id: "phone",
    label: "Phone",
    type: "text",
    placeholder: "+1 234 567 89",
    require: true,
  },
  {
    id: "password",
    label: "Password",
    type: "password",
  },
  {
    id: "country",
    label: "Country",
    type: "text",
    placeholder: "Country",
    require: true,
  },
  {
    id: "city",
    label: "City",
    type: "text",
    placeholder: "City",
    require: true,
  },
];

export const houseInputs = [
  {
    id: "name",
    label: "Name",
    type: "text",
    placeholder: "My house",
  },
  {
    id: "type",
    label: "Type",
    type: "text",
    placeholder: "house",
  },
  {
    id: "city",
    label: "City",
    type: "text",
    placeholder: "City",
  },
  {
    id: "address",
    label: "Address",
    type: "text",
    placeholder: "Address",
  },
  {
    id: "distance",
    label: "Distance from City Center",
    type: "text",
    placeholder: "500",
  },
  {
    id: "title",
    label: "Title",
    type: "text",
    placeholder: "Title",
  },
  {
    id: "desc",
    label: "Description",
    type: "text",
    placeholder: "description",
  },
  {
    id: "cheapestPrice",
    label: "Price",
    type: "text",
    placeholder: "100",
  },
];

export const roomInputs = [
  {
    id: "title",
    label: "Title",
    type: "text",
    placeholder: "2 bed room",
  },
  {
    id: "desc",
    label: "Description",
    type: "text",
    placeholder: "King size bed, 1 bathroom",
  },
  {
    id: "price",
    label: "Price",
    type: "number",
    placeholder: "100",
  },
  {
    id: "maxPeople",
    label: "Max People",
    type: "number",
    placeholder: "2",
  },
];
