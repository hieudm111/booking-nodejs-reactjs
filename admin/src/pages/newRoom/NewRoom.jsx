/* eslint-disable no-unused-vars */
import "./newRoom.scss";
import Sidebar from "../../components/sidebar/Sidebar";
import Navbar from "../../components/navbar/Navbar";
import { useEffect, useState } from "react";
import useFetch from "../../hooks/useFetch";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { IoIosClose } from "react-icons/io";

const NewRoom = ({ inputs, title }) => {
  const [info, setInfo] = useState({});
  const [houseId, setHouseId] = useState(undefined);
  const [rooms, setRooms] = useState([]);
  const [displayError, setDisplayError] = useState(false);
  const [displayErrorBlank, setDisplayErrorBlank] = useState(false);
  const [displaySuccess, setDisplaySuccess] = useState(false);
  const navigate = useNavigate();

  const { data, loading, error } = useFetch("/houses");

  const handleChange = (e) => {
    setInfo((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleClick = async (e) => {
    e.preventDefault();
    let roomNumbers;
    if (rooms.length === 0) {
      setDisplayErrorBlank(true);
    } else {
      roomNumbers = rooms.split(",").map((room) => ({ number: room }));
    }
    try {
      let createRoom;
      if (!Object.entries(info) || Object.entries(info).length < 4) {
        createRoom = null;
      } else {
        createRoom = await axios.post(`/rooms/${houseId}`, {
          ...info,
          roomNumbers,
        });
      }
      if (createRoom === null) {
        setDisplayErrorBlank(true);
      } else if (createRoom) {
        setDisplaySuccess(true);
        setTimeout(() => {
          navigate("/rooms");
        }, 3000);
      } else {
        setDisplayError(true);
      }
    } catch (err) {
      setDisplayError(true);
    }
  };

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 3000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayErrorBlank) {
      setTimeout(() => {
        setDisplayErrorBlank(false);
      }, 3000);
    }
  }, [displayErrorBlank]);

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 2000);
    }
  }, [displaySuccess]);

  return (
    <div className="new">
      <Sidebar />
      <div className="newContainer">
        <Navbar />
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">
          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              zIndex: "9999",
              backgroundColor: "red",
              padding: "20px",
              right: "-0.4%",
              top: "-25%",
              display: displayErrorBlank ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Error:
              </span>
              <span style={{ color: "white" }}>All field are required!</span>
            </div>
            <button
              onClick={() => setDisplayErrorBlank(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              zIndex: "9999",
              backgroundColor: "red",
              padding: "20px",
              right: "-0.4%",
              top: "-24%",
              display: displayError ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Error:
              </span>
              <span style={{ color: "white" }}>Create room is failure!</span>
            </div>
            <button
              onClick={() => setDisplayError(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              backgroundColor: "green",
              padding: "20px",
              right: "-0.4%",
              top: "-24%",
              display: displaySuccess ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Success:
              </span>
              <span style={{ color: "white" }}>Create Successfully!</span>
            </div>
            <button
              onClick={() => setDisplaySuccess(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>

          <div className="right">
            <form>
              {inputs.map((input) => (
                <div className="formInput" key={input.id}>
                  <label>{input.label}</label>
                  <input
                    onChange={handleChange}
                    type={input.type}
                    placeholder={input.placeholder}
                    id={input.id}
                  />
                </div>
              ))}
              <div className="formInput">
                <label>Rooms</label>
                <textarea
                  onChange={(e) => setRooms(e.target.value)}
                  placeholder="give coma between room numbers."
                />
              </div>
              <div className="formInput">
                <label>Choose a house</label>
                <select
                  id="houseId"
                  onChange={(e) => setHouseId(e.target.value)}
                >
                  <option style={{ fontStyle: "italic" }}>
                    -- Choose house to add room --
                  </option>
                  {loading
                    ? "loading"
                    : data &&
                      data.map((house) => (
                        <option key={house._id} value={house._id}>
                          {house.name}
                        </option>
                      ))}
                </select>
              </div>
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <button className="btnAddRoom" onClick={handleClick}>
                  Send
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewRoom;
