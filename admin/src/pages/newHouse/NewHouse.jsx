/* eslint-disable no-unused-vars */
import "./newHouse.scss";
import Sidebar from "../../components/sidebar/Sidebar";
import Navbar from "../../components/navbar/Navbar";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import { useEffect, useState } from "react";
import { houseInputs } from "../../formSource";
import useFetch from "../../hooks/useFetch";
import axios from "axios";
import { IoIosClose } from "react-icons/io";
import { useNavigate } from "react-router-dom";

const NewHouse = ({ inputs, title }) => {
  const [files, setFiles] = useState("");
  const [info, setInfo] = useState({});
  const [rooms, setRooms] = useState([]);
  const [displayError, setDisplayError] = useState(false);
  const [displayErrorBlank, setDisplayErrorBlank] = useState(false);
  const [displaySuccess, setDisplaySuccess] = useState(false);
  const navigate = useNavigate();

  const { data, loading, error } = useFetch("/rooms");

  const handleChange = (e) => {
    setInfo((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleSelect = (e) => {
    const value = Array.from(
      e.target.selectedOptions,
      (option) => option.value
    );
    setRooms(value);
  };

  const handleClick = async (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append("photos", files);
    try {
      const uploadRes = await axios.post("/houses/upload", data, {
        headers: { "Content-Type": "multipart/form-data" },
      });
      const filenames = uploadRes.data.toString().substring(1);
      if (rooms.length === 0) {
        setDisplayErrorBlank(true);
      } else {
        const newHouse = {
          ...info,
          rooms,
          photos: filenames,
        };
        const createHouse = await axios.post("/houses", newHouse);
        if (createHouse) {
          setDisplaySuccess(true);
          setTimeout(() => {
            navigate("/houses");
          }, 3000);
        } else {
          setDisplayErrorBlank(true);
        }
      }
    } catch (err) {
      setDisplayError(true);
    }
  };

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 3000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 2000);
    }
  }, [displaySuccess]);

  return (
    <div className="new">
      <Sidebar />
      <div className="newContainer">
        <Navbar />
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">
          <div className="left">
            <img
              src={
                files
                  ? URL.createObjectURL(files)
                  : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
              }
              alt=""
            />
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              zIndex: "9999",
              backgroundColor: "red",
              padding: "20px",
              right: "-0.4%",
              top: "-13%",
              display: displayErrorBlank ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Error:
              </span>
              <span style={{ color: "white" }}>All field are required!</span>
            </div>
            <button
              onClick={() => setDisplayErrorBlank(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              zIndex: "9999",
              backgroundColor: "red",
              padding: "20px",
              right: "-0.4%",
              top: "-13%",
              display: displayError ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Error:
              </span>
              <span style={{ color: "white" }}>Create house is failure!</span>
            </div>
            <button
              onClick={() => setDisplayError(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              backgroundColor: "green",
              padding: "20px",
              right: "-0.4%",
              top: "-13%",
              display: displaySuccess ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Success:
              </span>
              <span style={{ color: "white" }}>Create Successfully!</span>
            </div>
            <button
              onClick={() => setDisplaySuccess(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>
          <div className="right">
            <form>
              <div className="formInput">
                <label htmlFor="file">
                  Image: <DriveFolderUploadOutlinedIcon className="icon" />
                </label>
                <input
                  type="file"
                  id="file"
                  onChange={(e) => setFiles(e.target.files[0])}
                  style={{ display: "none" }}
                />
              </div>

              {inputs.map((input) => (
                <div className="formInput" key={input.id}>
                  <label>{input.label}</label>
                  <input
                    onChange={handleChange}
                    type={input.type}
                    placeholder={input.placeholder}
                    id={input.id}
                  />
                </div>
              ))}
              <div className="formInput">
                <label>Featured</label>
                <select id="featured" onChange={handleChange}>
                  <option value={false}>No</option>
                  <option value={true}>Yes</option>
                </select>
              </div>
              <div className="selectRooms">
                <label>Rooms</label>
                <select id="rooms" multiple onChange={handleSelect}>
                  {loading
                    ? "loading"
                    : data &&
                      data.map((room) => (
                        <option key={room._id} value={room._id}>
                          {room.title}
                        </option>
                      ))}
                </select>
              </div>
              <button onClick={handleClick}>Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewHouse;
