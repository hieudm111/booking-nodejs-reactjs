import "./new.scss";
import Sidebar from "../../components/sidebar/Sidebar";
import Navbar from "../../components/navbar/Navbar";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { IoIosClose } from "react-icons/io";

const New = ({ inputs, title }) => {
  const [file, setFile] = useState("");
  const [info, setInfo] = useState({});
  const [displayErrorBlank, setDisplayErrorBlank] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displaySuccess, setDisplaySuccess] = useState(false);
  const navigate = useNavigate();

  const handleChange = (e) => {
    setInfo((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleClick = async (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append("photos", file);
    try {
      const uploadRes = await axios.post("/users/upload", data, {
        headers: { "Content-Type": "multipart/form-data" },
      });

      const filenames = uploadRes.data.toString().substring(1);
      const newUser = {
        ...info,
        img: filenames,
      };

      let createUser;
      if (!Object.entries(info) || Object.entries(info).length < 6) {
        createUser = null;
      } else {
        createUser = await axios.post("/auth/register", newUser);
      }

      if (createUser) {
        setDisplaySuccess(true);
        setTimeout(() => {
          navigate("/users");
        }, 3000);
      } else if (!createUser) {
        setDisplayErrorBlank(true);
      } else {
        setDisplayError(true);
      }
    } catch (err) {
      setDisplayError(true);
    }
  };

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 3000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayErrorBlank) {
      setTimeout(() => {
        setDisplayErrorBlank(false);
      }, 3000);
    }
  }, [displayErrorBlank]);

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 2000);
    }
  }, [displaySuccess]);

  return (
    <div className="new">
      <Sidebar />
      <div className="newContainer">
        <Navbar />
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">
          <div className="left">
            <img
              src={
                file
                  ? URL.createObjectURL(file)
                  : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
              }
              alt=""
            />
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              zIndex: "9999",
              backgroundColor: "red",
              padding: "20px",
              right: "-0.4%",
              top: "-25%",
              display: displayErrorBlank ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Error:
              </span>
              <span style={{ color: "white" }}>All field are required!</span>
            </div>
            <button
              onClick={() => setDisplayErrorBlank(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              zIndex: "9999",
              backgroundColor: "red",
              padding: "20px",
              right: "-0.4%",
              top: "-25%",
              display: displayError ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Error:
              </span>
              <span style={{ color: "white" }}>Create user is failure!</span>
            </div>
            <button
              onClick={() => setDisplayError(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>

          <div
            style={{
              minWidth: "25%",
              position: "absolute",
              backgroundColor: "green",
              padding: "20px",
              right: "-0.4%",
              top: "-25%",
              display: displaySuccess ? "flex" : "none",
              justifyContent: "space-between",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <div
              style={{
                minHeight: "100%",
                display: "flex",
                alignItems: "center",
                gap: "10px",
              }}
            >
              <span
                style={{ color: "white", fontWeight: "bold", fontSize: "18px" }}
              >
                Success:
              </span>
              <span style={{ color: "white" }}>Create Successfully!</span>
            </div>
            <button
              onClick={() => setDisplaySuccess(false)}
              style={{
                cursor: "pointer",
                border: "none",
                padding: "0",
                background: "none",
                fontSize: "30px",
                display: "flex",
                justifyContent: "flex-end",
                color: "white",
              }}
            >
              <IoIosClose />
            </button>
          </div>
          <div className="right">
            <form>
              <div className="formInput">
                <label htmlFor="file">
                  Image: <DriveFolderUploadOutlinedIcon className="icon" />
                </label>
                <input
                  type="file"
                  id="file"
                  onChange={(e) => setFile(e.target.files[0])}
                  style={{ display: "none" }}
                />
              </div>

              {inputs.map((input) => (
                <div className="formInput" key={input.id}>
                  <label>{input.label}</label>
                  <input
                    onChange={handleChange}
                    type={input.type}
                    placeholder={input.placeholder}
                    id={input.id}
                  />
                </div>
              ))}
              <button onClick={handleClick}>Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default New;
